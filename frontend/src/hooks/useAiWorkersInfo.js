import axios from 'axios';
import { useQuery } from 'react-query';

export const useAiWorkersInfo = () => {
  const fetchAiWorkers = async () => {
    const aiWorkers = await Promise.all([
      {
        id: 1,
        name: 'AI Worker 1',
        description: 'This AI worker specializes in data analysis and pattern recognition.',
        imageUrl: 'https://example.com/ai-worker-1.jpg'
      },
      {
        id: 2,
        name: 'AI Worker 2',
        description: 'Focused on natural language processing and machine learning.',
        imageUrl: 'https://example.com/ai-worker-2.jpg'
      },
      {
        id: 3,
        name: 'AI Worker 3',
        description: 'Expert in robotics and autonomous systems.',
        imageUrl: 'https://example.com/ai-worker-3.jpg'
      }
    ].map(async (worker) => {
      try {
        await axios.get(worker.imageUrl);
        return worker;
      } catch (error) {
        return {
          ...worker,
          imageUrl: 'https://example.com/default-placeholder.jpg'
        };
      }
    }));

    return aiWorkers;
  };

  const { data, status } = useQuery('aiWorkersInfo', fetchAiWorkers);

  return { data, status };
};