import { Box, VStack, Heading, Button, Link, useColorModeValue, Center, Text, Image, HStack, Avatar, Flex } from '@chakra-ui/react';

const Home = () => {
  const bgColor = 'black';
  const textColor = useColorModeValue('whiteAlpha.900', 'gray.800');

  return (
    <>
      <Flex as="header" w="100%" zIndex="sticky" bg={bgColor} color={textColor} justifyContent="center" alignItems="center" py="2" px="4" position="sticky" top="0">
        <HStack spacing="4">
          <Image src="https://assets-global.website-files.com/63909f6fc71535ed58405374/6390b4eb9767826daa49cfaa_Groueep%201.svg" w="50px" alt="HF0 Logo" />
          <Text fontSize="sm" fontWeight="light">x</Text>
          <Image src="https://uploads-ssl.webflow.com/649081f0ae28305bdf0ff3b2/65de38c57979c909f1a69ab5_kortix-ai.png" w="70px" alt="Kortix.ai Logo" />
        </HStack>
      </Flex>
      <Center bg={bgColor} color={textColor} minH="100vh" py="12" px={{ base: '4', lg: '8' }}>
        <VStack spacing="8">
          <Image src="https://assets-global.website-files.com/63909f6fc71535ed58405374/6391bf578a629c68a6df7bdf_01.-Sliced-sphere_25-fps_small.gif" alt="Sliced Sphere" boxSize={{ base: '150px', md: '200px' }} my={4} />
          <Box px={{ base: '4', md: '8', lg: '12' }}>
            <Heading as="h2" size="sm" textAlign="center" color="purple.500">
              You said I should pick one URL, but I had multiple, so I had my Dev Agent build this website quickly to combine them into one.
            </Heading>
            <Heading as="h1" size="lg" textAlign="center" my="4">
              <Avatar src="https://media.licdn.com/dms/image/D4E03AQEEIDe7V9k_9w/profile-displayphoto-shrink_800_800/0/1687302199230?e=1714003200&v=beta&t=gGWKFoFYqmzCm7n1ENscWlplF9lKXWEGK3uwoua0L7k" size="md" mr="2" />
              I am building: <Text as="span" color="purple.500">1.</Text> The AI Developer powered by the <Text as="span" color="purple.500">2.</Text> OACE Framework. To create the <Text as="span" color="purple.500">3.</Text> leading AI Agent Infrastructure company that enables the migration from a human-based to an AI workforce across all domains.
            </Heading>
          </Box>
          <Text textAlign="center" mb="4">
            Explore the links below to learn more:
          </Text>
          <Button as={Link} href="https://github.com/markokraemer/oace/blob/main/framework.md" colorScheme="purple" size="lg" isExternal>
            OACE Framework
          </Button>
          <Button as={Link} href="https://x.com/markokraemer/status/1750252512035139767?s=20" colorScheme="purple" size="lg" isExternal>
            AI Developer
          </Button>
          <Button as={Link} href="#pitchDeck" colorScheme="purple" size="lg">
            WIP Pitch Deck
          </Button>
          <Heading as="h2" size="lg" textAlign="center" my="4">
            The work-in-progress pitch deck for Kortix.ai.
          </Heading>
          <Box as="iframe" id="pitchDeck" style={{ border: '1px solid rgba(255, 255, 255, 0.1)' }} width="100vw" height="100vh" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fproto%2Fo1hZgCZXCN3uah8877GYwT%2FKortix.ai%3Fpage-id%3D158%253A1012%26type%3Ddesign%26node-id%3D365-1641%26viewport%3D465%252C368%252C0.02%26t%3DyUGiI8YHckAyRNcw-1%26scaling%3Dcontain%26mode%3Ddesign" allowFullScreen />
        </VStack>
      </Center>
    </>
  );
};

export default Home;