import React from 'react';
import { Box, Heading, Text, Flex, Button, useBreakpointValue, Container, VStack } from '@chakra-ui/react';
import { AiWorkersShowcase } from '@components/AiWorkersShowcase';

const Example = () => {
  const headingSize = useBreakpointValue({ base: '2xl', md: '4xl', lg: '5xl' });
  const textSize = useBreakpointValue({ base: 'md', md: 'lg', lg: 'xl' });

  return (
    <Box>
      <Flex
        direction='column'
        align='center'
        justify='center'
        h={{ base: '50vh', md: '70vh' }}
        bgGradient='linear(to-r, teal.500, green.500)'
        color='white'
        textAlign='center'
        p={4}
      >
        <VStack spacing={4}>
          <Heading as='h1' size={headingSize}>
            Revolutionize Your Work with AI Workers
          </Heading>
          <Text fontSize={textSize}>
            Discover how AI Workers can transform your business operations, making them more efficient and innovative.
          </Text>
          <Button colorScheme='teal' size='lg'>Learn More</Button>
        </VStack>
      </Flex>
      <Container maxW='container.xl' py={10}>
        <AiWorkersShowcase />
      </Container>
    </Box>
  );
};

export default Example;