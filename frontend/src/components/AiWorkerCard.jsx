import { Box, Image, Text, Heading } from '@chakra-ui/react';

export const AiWorkerCard = ({ name, description, imageUrl }) => {
  return (
    <Box maxW='sm' borderWidth='1px' borderRadius='lg' overflow='hidden' p={5} boxShadow='lg' bgGradient='linear(to-r, teal.500, green.500)'>
      <Image src={imageUrl} alt={`Image of ${name}`} borderRadius='full' boxSize='100px' objectFit='cover' mx='auto' my={4} />
      <Heading as='h3' size='lg' textAlign='center' color='white'>{name}</Heading>
      <Text mt={2} fontSize='md' textAlign='center' color='gray.200'>{description}</Text>
    </Box>
  );
};