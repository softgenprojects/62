import { SimpleGrid, Spinner, Text, Center } from '@chakra-ui/react';
import { AiWorkerCard } from '@components/AiWorkerCard';
import { useAiWorkersInfo } from '@hooks/useAiWorkersInfo';

export const AiWorkersShowcase = () => {
  const { data: aiWorkers, status } = useAiWorkersInfo();

  if (status === 'loading') {
    return (
      <Center h='100vh'>
        <Spinner size='xl' />
      </Center>
    );
  }

  if (status === 'error') {
    return (
      <Text fontSize='xl' textAlign='center'>Failed to load AI Workers information.</Text>
    );
  }

  return (
    <SimpleGrid columns={{ base: 1, md: 2, lg: 3 }} spacing={10} p={10}>
      {aiWorkers.map((worker) => (
        <AiWorkerCard key={worker.id} name={worker.name} description={worker.description} imageUrl={worker.imageUrl} />
      ))}
    </SimpleGrid>
  );
};